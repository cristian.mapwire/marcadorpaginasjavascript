# MarcadorPaginasJavascript

Consiste en un sistema de guardado sencillo de URLs de paginas web que deseemos visitar más tarde.
Solo admite URLs, y nos permite también eliminar las URLs que no deseemos que estén en nuestro listado de páginas guardadas.